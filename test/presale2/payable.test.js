const { BN, time, expectRevert} = require('@openzeppelin/test-helpers');
const { expect } = require('chai');

const MoniToken = artifacts.require('MoniToken');
const MoniPresale2 = artifacts.require('MoniPresale2');

contract('payable', function (accounts) {
  const [ 
    owner,
    otherAddress, 
    address1,
    address2,
    address3,
    address4,
    address5,
    address6,
    address7,
    address8,
  ] = accounts;

  beforeEach(async function () {
    this.moniToken = await MoniToken.new(otherAddress, otherAddress, otherAddress, otherAddress, otherAddress, otherAddress);
    this.moniPresale2 = await MoniPresale2.new(this.moniToken.address)
    await this.moniToken.setPreSale2ContractNotYetSet(this.moniPresale2.address);

    await this.moniPresale2.addDepositAddress(
      [address1, address2, address3, address4, address5, address6, address7, address8],
      { from: owner, value: '0', gas: '5000000' });
  });

  // it('Should not allow deposit if preSale2 has not yet started', async function () {
  //   const options = { 
  //     from: otherAddress, 
  //     to : this.moniPresale2.address, 
  //     value: web3.utils.toWei('1', 'ether')
  //   }
  //   await expectRevert(this.moniPresale2.sendTransaction(options), "Error: Returned error: VM Exception while processing transaction: revert Deposit rejected, presale2 has either not yet started or not yet overed -- Reason given: Deposit rejected, presale2 has either not yet started or not yet overed.");
  // });

  // it('Should not allow deposit if address is not whitelist', async function () {
  //   await time.increaseTo(1631664001) // Set to 1 minute just after preSale2 started
  //   const options = { 
  //     from: otherAddress, 
  //     to : this.moniPresale2.address, 
  //     value: web3.utils.toWei('1', 'ether')
  //   }

  //   await expectRevert(this.moniPresale2.sendTransaction(options), "Deposit rejected, deposit address is not yet whitelisted -- Reason given: Deposit rejected, deposit address is not yet whitelisted.");
  // });

  // it('Should not allow deposit if shouldPresale2EndEarlier is true', async function () {
  //   await time.increaseTo(1631664001) // Set to 1 minute just after preSale2 started
    
  //   await this.moniPresale2.endPreSale2Earlier(
  //     { from: owner, value: '0', gas: '5000000' });

  //   const options = { 
  //     from: address1, 
  //     to : this.moniPresale2.address, 
  //     value: web3.utils.toWei('1', 'ether')
  //   }

  //   await expectRevert(this.moniPresale2.sendTransaction(options), "Admin has ended presale2 earlier -- Reason given: Admin has ended presale2 earlier.");  
  // });

  // it('Should not allow deposit if not reach minimum deposit', async function () {
  //   await time.increaseTo(1631664001) // Set to 1 minute just after preSale2 started
    
  //   const options = { 
  //     from: address1, 
  //     to : this.moniPresale2.address, 
  //     value: web3.utils.toWei('0.09', 'ether')
  //   }

  //   await expectRevert(this.moniPresale2.sendTransaction(options), "Deposit rejected, it is lesser than minimum amount -- Reason given: Deposit rejected, it is lesser than minimum amount.");  
  // });

  it('Should allow deposit', async function () {
    await time.increaseTo(1631664001) // Set to 1 minute just after preSale2 started
    
    {
      const options = { 
        from: address1, 
        to : this.moniPresale2.address, 
        value: web3.utils.toWei('0.1', 'ether')
      }

      await this.moniPresale2.sendTransaction(options)
    }

    {
      const options = { 
        from: address2, 
        to : this.moniPresale2.address, 
        value: web3.utils.toWei('0.2', 'ether')
      }

      await this.moniPresale2.sendTransaction(options)
    }

    let address1DepositAddressAmount = await this.moniPresale2._depositAddressesBNBAmount(address1);
    let address2DepositAddressAmount = await this.moniPresale2._depositAddressesBNBAmount(address2);
    let totalDepositAmount = await this.moniPresale2._totalAddressesDepositAmount();

    expect(address1DepositAddressAmount).to.be.bignumber.equal(new BN('100000000000000000'));
    expect(address2DepositAddressAmount).to.be.bignumber.equal(new BN('200000000000000000'));
    expect(totalDepositAmount).to.be.bignumber.equal(new BN('300000000000000000'));
  });
});
