const { BN, time, expectRevert} = require('@openzeppelin/test-helpers');
const { expect } = require('chai');

const MoniToken = artifacts.require('MoniToken');
const MoniPresale2 = artifacts.require('MoniPresale2');

contract('payable', function (accounts) {
  const [ 
    owner,
    otherAddress, 
    address1,
    address2,
    address3,
    address4,
    address5,
    address6,
    address7,
    address8,
  ] = accounts;

  before(async function () {
    this.moniToken = await MoniToken.new(otherAddress, otherAddress, otherAddress, otherAddress, otherAddress, otherAddress);
    this.moniPresale2 = await MoniPresale2.new(this.moniToken.address)
    await this.moniToken.setPreSale2ContractNotYetSet(this.moniPresale2.address);

    await this.moniPresale2.addDepositAddress(
      [address1, address2, address3, address4, address5, address6, address7, address8],
      { from: owner, value: '0', gas: '5000000' });

      await time.increaseTo(1631664001) // Set to 1 minute just after preSale2 started
    
      {
        await this.moniPresale2.sendTransaction({ 
          from: address1, 
          to : this.moniPresale2.address, 
          value: web3.utils.toWei('10', 'ether')
        })

        await this.moniPresale2.sendTransaction({ 
          from: address2, 
          to : this.moniPresale2.address, 
          value: web3.utils.toWei('30', 'ether')
        })

        await this.moniPresale2.sendTransaction({ 
          from: address3, 
          to : this.moniPresale2.address, 
          value: web3.utils.toWei('50', 'ether')
        })

        await this.moniPresale2.sendTransaction({ 
          from: address4, 
          to : this.moniPresale2.address, 
          value: web3.utils.toWei('70', 'ether')
        })

        await this.moniPresale2.sendTransaction({ 
          from: address5, 
          to : this.moniPresale2.address, 
          value: web3.utils.toWei('90', 'ether')
        })

        await this.moniPresale2.sendTransaction({ 
          from: address6, 
          to : this.moniPresale2.address, 
          value: web3.utils.toWei('90', 'ether')
        })

        await this.moniPresale2.sendTransaction({ 
          from: address7, 
          to : this.moniPresale2.address, 
          value: web3.utils.toWei('70', 'ether')
        })

        await this.moniPresale2.sendTransaction({ 
          from: address8, 
          to : this.moniPresale2.address, 
          value: web3.utils.toWei('50', 'ether')
        })
      }

      await time.increaseTo(1631728801) // Set to 1 minute just after preSale2 ended
  });

  it('Should distribute 1', async function () {
    await time.increaseTo(1631815201) // Set to 1 minute just after distribute1 date

    let totalDepositAmount = await this.moniPresale2._totalAddressesDepositAmount();
    expect(totalDepositAmount).to.be.bignumber.equal(new BN('460000000000000000000'));
   
    await this.moniPresale2.distributeFirst(8, { from: owner, value: '0', gas: '5000000' });
   
    const address1ERC20Balance = await this.moniToken.balanceOf(address1)
    const address2ERC20Balance = await this.moniToken.balanceOf(address2)
    const address3ERC20Balance = await this.moniToken.balanceOf(address3)
    const address4ERC20Balance = await this.moniToken.balanceOf(address4)
    const address5ERC20Balance = await this.moniToken.balanceOf(address5)
    const address6ERC20Balance = await this.moniToken.balanceOf(address6)
    const address7ERC20Balance = await this.moniToken.balanceOf(address7)
    const address8ERC20Balance = await this.moniToken.balanceOf(address8)

    console.log(address1ERC20Balance.toString())
    console.log(address2ERC20Balance.toString())
    console.log(address3ERC20Balance.toString())
    console.log(address4ERC20Balance.toString())
    console.log(address5ERC20Balance.toString())
    console.log(address6ERC20Balance.toString())
    console.log(address7ERC20Balance.toString())
    console.log(address8ERC20Balance.toString())
  });
});
