const { BN, time,} = require('@openzeppelin/test-helpers');
const { expect } = require('chai');

const MoniToken = artifacts.require('MoniToken');
const MoniPresale2 = artifacts.require('MoniPresale2');

contract('RemoveDepositAddress', function (accounts) {
  const [ 
    owner,
    otherAddress, 
    address1,
    address2,
    address3,
    address4,
    address5,
    address6,
    address7,
    address8,
  ] = accounts;

  beforeEach(async function () {
    this.moniToken = await MoniToken.new(otherAddress, otherAddress, otherAddress, otherAddress, otherAddress, otherAddress);
    this.moniPresale2 = await MoniPresale2.new(this.moniToken.address)
    await this.moniToken.setPreSale2ContractNotYetSet(this.moniPresale2.address);

    await this.moniPresale2.addDepositAddress(
      [address1, address2, address3, address4, address5, address6, address7, address8],
      { from: owner, value: '0', gas: '5000000' });
  });

  it('Should remove all deposit if number = deposit number', async function () {
    await this.moniPresale2.removeAllDepositAddress(8);

    let balanceAddress1 = await this.moniPresale2._depositAddressesStatus(address1);
    let balanceAddress2 = await this.moniPresale2._depositAddressesStatus(address2);
    let balanceAddress3 = await this.moniPresale2._depositAddressesStatus(address3);
    let balanceAddress4 = await this.moniPresale2._depositAddressesStatus(address4);
    let balanceAddress5 = await this.moniPresale2._depositAddressesStatus(address5);
    let balanceAddress6 = await this.moniPresale2._depositAddressesStatus(address6);
    let balanceAddress7 = await this.moniPresale2._depositAddressesStatus(address7);
    let balanceAddress8 = await this.moniPresale2._depositAddressesStatus(address8);

    expect(balanceAddress1).to.equal(false);
    expect(balanceAddress2).to.equal(false);
    expect(balanceAddress3).to.equal(false);
    expect(balanceAddress4).to.equal(false);
    expect(balanceAddress5).to.equal(false);
    expect(balanceAddress6).to.equal(false);
    expect(balanceAddress7).to.equal(false);
    expect(balanceAddress8).to.equal(false);

    const firstIndex = await this.moniPresale2._distributeFirstIndex()
    const secondIndex = await this.moniPresale2._distributeSecondIndex()
    const thirdIndex = await this.moniPresale2._distributeThirdIndex()
    const depositAddressIndex = await this.moniPresale2._startDepositAddressIndex()

    expect(firstIndex).to.be.bignumber.equal(new BN('8'));
    expect(secondIndex).to.be.bignumber.equal(new BN('8'));
    expect(thirdIndex).to.be.bignumber.equal(new BN('8'));
    expect(depositAddressIndex).to.be.bignumber.equal(new BN('8'));
  });

  it('Should remove all deposit if number < deposit number', async function () {
    await this.moniPresale2.removeAllDepositAddress(7);

    let balanceAddress1 = await this.moniPresale2._depositAddressesStatus(address1);
    let balanceAddress2 = await this.moniPresale2._depositAddressesStatus(address2);
    let balanceAddress3 = await this.moniPresale2._depositAddressesStatus(address3);
    let balanceAddress4 = await this.moniPresale2._depositAddressesStatus(address4);
    let balanceAddress5 = await this.moniPresale2._depositAddressesStatus(address5);
    let balanceAddress6 = await this.moniPresale2._depositAddressesStatus(address6);
    let balanceAddress7 = await this.moniPresale2._depositAddressesStatus(address7);
    let balanceAddress8 = await this.moniPresale2._depositAddressesStatus(address8);

    expect(balanceAddress1).to.equal(false);
    expect(balanceAddress2).to.equal(false);
    expect(balanceAddress3).to.equal(false);
    expect(balanceAddress4).to.equal(false);
    expect(balanceAddress5).to.equal(false);
    expect(balanceAddress6).to.equal(false);
    expect(balanceAddress7).to.equal(false);
    expect(balanceAddress8).to.equal(true);

    const firstIndex = await this.moniPresale2._distributeFirstIndex()
    const secondIndex = await this.moniPresale2._distributeSecondIndex()
    const thirdIndex = await this.moniPresale2._distributeThirdIndex()
    const depositAddressIndex = await this.moniPresale2._startDepositAddressIndex()

    expect(firstIndex).to.be.bignumber.equal(new BN('7'));
    expect(secondIndex).to.be.bignumber.equal(new BN('7'));
    expect(thirdIndex).to.be.bignumber.equal(new BN('7'));
    expect(depositAddressIndex).to.be.bignumber.equal(new BN('7'));
  });

  it('Should remove all deposit if number > deposit number', async function () {
    await this.moniPresale2.removeAllDepositAddress(9);

    let balanceAddress1 = await this.moniPresale2._depositAddressesStatus(address1);
    let balanceAddress2 = await this.moniPresale2._depositAddressesStatus(address2);
    let balanceAddress3 = await this.moniPresale2._depositAddressesStatus(address3);
    let balanceAddress4 = await this.moniPresale2._depositAddressesStatus(address4);
    let balanceAddress5 = await this.moniPresale2._depositAddressesStatus(address5);
    let balanceAddress6 = await this.moniPresale2._depositAddressesStatus(address6);
    let balanceAddress7 = await this.moniPresale2._depositAddressesStatus(address7);
    let balanceAddress8 = await this.moniPresale2._depositAddressesStatus(address8);

    expect(balanceAddress1).to.equal(false);
    expect(balanceAddress2).to.equal(false);
    expect(balanceAddress3).to.equal(false);
    expect(balanceAddress4).to.equal(false);
    expect(balanceAddress5).to.equal(false);
    expect(balanceAddress6).to.equal(false);
    expect(balanceAddress7).to.equal(false);
    expect(balanceAddress8).to.equal(false);

    const firstIndex = await this.moniPresale2._distributeFirstIndex()
    const secondIndex = await this.moniPresale2._distributeSecondIndex()
    const thirdIndex = await this.moniPresale2._distributeThirdIndex()
    const depositAddressIndex = await this.moniPresale2._startDepositAddressIndex()

    expect(firstIndex).to.be.bignumber.equal(new BN('8'));
    expect(secondIndex).to.be.bignumber.equal(new BN('8'));
    expect(thirdIndex).to.be.bignumber.equal(new BN('8'));
    expect(depositAddressIndex).to.be.bignumber.equal(new BN('8'));
  });

  it('Should not remove deposit if date > preSale date', async function () {
    await time.increaseTo(1631728801)
    try {
      await this.moniPresale2.removeAllDepositAddress(8);
    } catch (e) {
      return true;
    }
    throw new Error("Error: Returned error: VM Exception while processing transaction: revert Presale2 already started -- Reason given: Presale2 already started.")
  });
});