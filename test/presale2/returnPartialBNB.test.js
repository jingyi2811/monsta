const { BN, time, expectRevert} = require('@openzeppelin/test-helpers');
const { expect } = require('chai');

const MoniToken = artifacts.require('MoniToken');
const MoniPresale2 = artifacts.require('MoniPresale2');

contract('payable', function (accounts) {
  const [ 
    owner,
    otherAddress, 
    address1,
    address2,
    address3,
    address4,
    address5,
    address6,
    address7,
    address8,
  ] = accounts;

  before(async function () {
    this.moniToken = await MoniToken.new(otherAddress, otherAddress, otherAddress, otherAddress, otherAddress, otherAddress);
    this.moniPresale2 = await MoniPresale2.new(this.moniToken.address)
    await this.moniToken.setPreSale2ContractNotYetSet(this.moniPresale2.address);

    await this.moniPresale2.addDepositAddress(
      [address1, address2, address3, address4, address5, address6, address7, address8],
      { from: owner, value: '0', gas: '5000000' });

      await time.increaseTo(1631664001) // Set to 1 minute just after preSale2 started
    
      {
        await this.moniPresale2.sendTransaction({ 
          from: address1, 
          to : this.moniPresale2.address, 
          value: web3.utils.toWei('10', 'ether')
        })

        await this.moniPresale2.sendTransaction({ 
          from: address2, 
          to : this.moniPresale2.address, 
          value: web3.utils.toWei('30', 'ether')
        })

        await this.moniPresale2.sendTransaction({ 
          from: address3, 
          to : this.moniPresale2.address, 
          value: web3.utils.toWei('50', 'ether')
        })

        await this.moniPresale2.sendTransaction({ 
          from: address4, 
          to : this.moniPresale2.address, 
          value: web3.utils.toWei('70', 'ether')
        })

        await this.moniPresale2.sendTransaction({ 
          from: address5, 
          to : this.moniPresale2.address, 
          value: web3.utils.toWei('90', 'ether')
        })

        await this.moniPresale2.sendTransaction({ 
          from: address6, 
          to : this.moniPresale2.address, 
          value: web3.utils.toWei('90', 'ether')
        })

        await this.moniPresale2.sendTransaction({ 
          from: address7, 
          to : this.moniPresale2.address, 
          value: web3.utils.toWei('70', 'ether')
        })

        await this.moniPresale2.sendTransaction({ 
          from: address8, 
          to : this.moniPresale2.address, 
          value: web3.utils.toWei('50', 'ether')
        })
      }

      await time.increaseTo(1631728801) // Set to 1 minute just after preSale2 ended
  });

  it('Should return BNB', async function () {
    let totalDepositAmount = await this.moniPresale2._totalAddressesDepositAmount();
    expect(totalDepositAmount).to.be.bignumber.equal(new BN('460000000000000000000'));
 
    const address1BalanceBefore = await web3.eth.getBalance(address1)
    const address2BalanceBefore = await web3.eth.getBalance(address2)
    const address3BalanceBefore = await web3.eth.getBalance(address3)
    const address4BalanceBefore = await web3.eth.getBalance(address4)
    const address5BalanceBefore = await web3.eth.getBalance(address5)
    const address6BalanceBefore = await web3.eth.getBalance(address6)
    const address7BalanceBefore = await web3.eth.getBalance(address7)
    const address8BalanceBefore = await web3.eth.getBalance(address8)

    await this.moniPresale2.returnBNB(1, { from: owner, value: '0', gas: '5000000' });
    await this.moniPresale2.returnBNB(2, { from: otherAddress, value: '0', gas: '5000000' });
    await this.moniPresale2.returnBNB(3, { from: address1, value: '0', gas: '5000000' });
    await this.moniPresale2.returnBNB(4, { from: address2, value: '0', gas: '5000000' });
   
    const address1BalanceAfter = await web3.eth.getBalance(address1)
    const address2BalanceAfter = await web3.eth.getBalance(address2)
    const address3BalanceAfter = await web3.eth.getBalance(address3)
    const address4BalanceAfter = await web3.eth.getBalance(address4)
    const address5BalanceAfter = await web3.eth.getBalance(address5)
    const address6BalanceAfter = await web3.eth.getBalance(address6)
    const address7BalanceAfter = await web3.eth.getBalance(address7)
    const address8BalanceAfter = await web3.eth.getBalance(address8)

    const address1BalanceFinal = address1BalanceAfter - address1BalanceBefore
    const address2BalanceFinal = address2BalanceAfter - address2BalanceBefore
    const address3BalanceFinal = address3BalanceAfter - address3BalanceBefore
    const address4BalanceFinal = address4BalanceAfter - address4BalanceBefore
    const address5BalanceFinal = address5BalanceAfter - address5BalanceBefore
    const address6BalanceFinal = address6BalanceAfter - address6BalanceBefore
    const address7BalanceFinal = address7BalanceAfter - address7BalanceBefore
    const address8BalanceFinal = address8BalanceAfter - address8BalanceBefore

    console.log(address1BalanceFinal)
    console.log(address2BalanceFinal)
    console.log(address3BalanceFinal)
    console.log(address4BalanceFinal)
    console.log(address5BalanceFinal)
    console.log(address6BalanceFinal)
    console.log(address7BalanceFinal)
    console.log(address8BalanceFinal)
    
    const contractBalance = await web3.eth.getBalance(this.moniPresale2.address)
    console.log(contractBalance)
  });
});
