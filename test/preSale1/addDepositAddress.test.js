const { BN, time,} = require('@openzeppelin/test-helpers');
const { expect } = require('chai');

const MoniToken = artifacts.require('MoniToken');
const MoniPresale1 = artifacts.require('MoniPresale1');

contract('addDepositAddress', function (accounts) {
  const [ 
    owner,
    otherAddress, 
    address1,
    address2,
    address3,
    address4,
    address5,
    address6,
    address7,
    address8,
  ] = accounts;

  before(async function () {
    this.moniToken = await MoniToken.new(otherAddress, otherAddress, otherAddress, otherAddress, otherAddress, otherAddress);
    this.moniPresale1 = await MoniPresale1.new(this.moniToken.address)
    
    await this.moniPresale1.addDepositAddress(
      [address1, address2, address3, address4, address5, address6, address7, address8],
      { from: owner, value: '0', gas: '5000000' });
  });

  it('Should get correct address number and address balance', async function () {
    let stakingTimes = await this.moniPresale1._depositAddressesNumber();
    expect(stakingTimes).to.be.bignumber.equal(new BN('8'));

    let balanceAddress1 = await this.moniPresale1._depositAddressesStatus(address1);
    let balanceAddress2 = await this.moniPresale1._depositAddressesStatus(address2);
    let balanceAddress3 = await this.moniPresale1._depositAddressesStatus(address3);
    let balanceAddress4 = await this.moniPresale1._depositAddressesStatus(address4);
    let balanceAddress5 = await this.moniPresale1._depositAddressesStatus(address5);
    let balanceAddress6 = await this.moniPresale1._depositAddressesStatus(address6);
    let balanceAddress7 = await this.moniPresale1._depositAddressesStatus(address7);
    let balanceAddress8 = await this.moniPresale1._depositAddressesStatus(address8);

    expect(balanceAddress1).to.equal(true);
    expect(balanceAddress2).to.equal(true);
    expect(balanceAddress3).to.equal(true);
    expect(balanceAddress4).to.equal(true);
    expect(balanceAddress5).to.equal(true);
    expect(balanceAddress6).to.equal(true);
    expect(balanceAddress7).to.equal(true);
    expect(balanceAddress8).to.equal(true);

    // Check for exception
    let other = await this.moniPresale1._depositAddressesStatus(otherAddress);
    expect(other).to.equal(false);
  });
});