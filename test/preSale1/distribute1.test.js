const { BN, time, expectRevert } = require('@openzeppelin/test-helpers');
const { expect } = require('chai');

const MoniToken = artifacts.require('MoniToken');
const MoniPresale1 = artifacts.require('MoniPresale1');

contract('payable', function (accounts) {
  const [
    owner,
    otherAddress,
    address1,
    address2,
    address3,
    address4,
    address5,
    address6,
    address7,
    address8,
  ] = accounts;

  before(async function () {
    this.moniToken = await MoniToken.new(otherAddress, otherAddress, otherAddress, otherAddress, otherAddress, otherAddress);
    this.moniPresale1 = await MoniPresale1.new(this.moniToken.address)
    await this.moniToken.setPreSale2ContractNotYetSet(this.moniPresale1.address);

    await this.moniPresale1.addDepositAddress(
      [address1, address2, address3, address4, address5, address6, address7, address8],
      { from: owner, value: '0', gas: '5000000' });

    await time.increaseTo(1631664001) // Set to 1 minute just after preSale2 started

    
    {
      const options = {
        from: address1,
        to: this.moniPresale1.address,
        value: web3.utils.toWei('50', 'ether')
      }

      await this.moniPresale1.sendTransaction(options)
    }

    {
      const options = {
        from: address2,
        to: this.moniPresale1.address,
        value: web3.utils.toWei('90', 'ether')
      }

      await this.moniPresale1.sendTransaction(options)
    }

    {
      const options = {
        from: address3,
        to: this.moniPresale1.address,
        value: web3.utils.toWei('75', 'ether')
      }

      await this.moniPresale1.sendTransaction(options)
    }

    {
      const options = {
        from: address4,
        to: this.moniPresale1.address,
        value: web3.utils.toWei('2', 'ether')
      }

      await this.moniPresale1.sendTransaction(options)
    }

    await time.increaseTo(1631577601) // Set to 1 minute just after preSale1 ended
  });

  it('Should distribute 1', async function () {
    await time.increaseTo(1631815201) // Set to 1 minute just after distribute1 date

    let totalDepositAmount = await this.moniPresale1._totalAddressesDepositAmount();
    expect(totalDepositAmount).to.be.bignumber.equal(new BN(web3.utils.toWei('200', 'ether')));

    await this.moniPresale1.distributeFirst(8, { from: address9, value: '0', gas: '5000000' });

    const address1ERC20Balance = await this.moniToken.balanceOf(address1)
    const address2ERC20Balance = await this.moniToken.balanceOf(address2)
    const address3ERC20Balance = await this.moniToken.balanceOf(address3)
    const address4ERC20Balance = await this.moniToken.balanceOf(address4)

    console.log(address1ERC20Balance.toString())
    console.log(address2ERC20Balance.toString())
    console.log(address3ERC20Balance.toString())
    console.log(address4ERC20Balance.toString())
  });
});