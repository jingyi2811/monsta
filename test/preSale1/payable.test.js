const { BN, time, expectRevert} = require('@openzeppelin/test-helpers');
const { expect } = require('chai');

const MoniToken = artifacts.require('MoniToken');
const MoniPresale1 = artifacts.require('MoniPresale1');

contract('payable', function (accounts) {
  const [ 
    owner,
    otherAddress, 
    address1,
    address2,
    address3,
    address4,
    address5,
    address6,
    address7,
    address8,
  ] = accounts;

  beforeEach(async function () {
    this.moniToken = await MoniToken.new(otherAddress, otherAddress, otherAddress, otherAddress, otherAddress, otherAddress);
    this.moniPresale1 = await MoniPresale1.new(this.moniToken.address)
    await this.moniToken.setPreSale1ContractNotYetSet(this.moniPresale1.address);

    await this.moniPresale1.addDepositAddress(
      [address1, address2, address3, address4, address5, address6, address7, address8],
      { from: owner, value: '0', gas: '5000000' });
  });

  // it('Should not allow deposit if preSale1 has not yet started', async function () {
  //   const options = { 
  //     from: otherAddress, 
  //     to : this.moniPresale1.address, 
  //     value: web3.utils.toWei('1', 'ether')
  //   }
  //   await expectRevert(this.moniPresale1.sendTransaction(options), "Error: Returned error: VM Exception while processing transaction: revert Deposit rejected, presale1
  // });

  // it('Should not allow deposit if address is not whitelist', async function () {
  //   await time.increaseTo(1631059200) // Set to 1 minute just after preSale1 started
  //   const options = { 
  //     from: otherAddress, 
  //     to : this.moniPresale1.address, 
  //     value: web3.utils.toWei('1', 'ether')
  //   }

  //   await expectRevert(this.moniPresale1.sendTransaction(options), "Deposit rejected, deposit address is not yet whitelisted -- Reason given: Deposit rejected, deposit address is not yet whitelisted.");
  // });

  // it('Should not allow deposit if larger than maximum deposit', async function () {
  //   await time.increaseTo(1631059200) // Set to 1 minute just after preSale1 started
    
  //   const options = { 
  //     from: address1, 
  //     to : this.moniPresale1.address, 
  //     value: web3.utils.toWei('11', 'ether')
  //   }

  //   await expectRevert(this.moniPresale1.sendTransaction(options), "Deposit rejected, it is more than maximum amount -- Reason given: Deposit rejected, it is more than maximum amount.");
  // });

  // it('Should not allow deposit if not reach minimum deposit', async function () {
  //   await time.increaseTo(1631059200) // Set to 1 minute just after preSale1 started
    
  //   const options = { 
  //     from: address1, 
  //     to : this.moniPresale1.address, 
  //     value: web3.utils.toWei('0.9', 'ether')
  //   }

  //   await expectRevert(this.moniPresale1.sendTransaction(options), "Deposit rejected, it is lesser than minimum amount -- Reason given: Deposit rejected, it is lesser than minimum amount.");  
  // });

  // it('Should allow deposit', async function () {
  //   await time.increaseTo(1631059200) // Set to 1 minute just after preSale1 started
    
  //   {
  //     const options = { 
  //       from: address1, 
  //       to : this.moniPresale1.address, 
  //       value: web3.utils.toWei('1', 'ether')
  //     }

  //     await this.moniPresale1.sendTransaction(options)
  //   }

  //   {
  //     const options = { 
  //       from: address2, 
  //       to : this.moniPresale1.address, 
  //       value: web3.utils.toWei('2', 'ether')
  //     }

  //     await this.moniPresale1.sendTransaction(options)
  //   }

  //   let address1DepositAddressAmount = await this.moniPresale1._depositAddressesBNBAmount(address1);
  //   let address2DepositAddressAmount = await this.moniPresale1._depositAddressesBNBAmount(address2);
  //   let totalDepositAmount = await this.moniPresale1._totalAddressesDepositAmount();

  //   expect(address1DepositAddressAmount).to.be.bignumber.equal(new BN(web3.utils.toWei('1', 'ether')));
  //   expect(address2DepositAddressAmount).to.be.bignumber.equal(new BN(web3.utils.toWei('2', 'ether')));
  //   expect(totalDepositAmount).to.be.bignumber.equal(new BN(web3.utils.toWei('3', 'ether')));
  // });

  // it('Should allow deposit although total deposit more than 200 bnb', async function () {
  //   await time.increaseTo(1631059200) // Set to 1 minute just after preSale1 started
    
  //   {
  //     const options = { 
  //       from: address1, 
  //       to : this.moniPresale1.address, 
  //       value: web3.utils.toWei('50', 'ether')
  //     }

  //     await this.moniPresale1.sendTransaction(options)
  //   }

  //   {
  //     const options = { 
  //       from: address2, 
  //       to : this.moniPresale1.address, 
  //       value: web3.utils.toWei('100', 'ether')
  //     }

  //     await this.moniPresale1.sendTransaction(options)
  //   }

  //   {
  //     const options = { 
  //       from: address3, 
  //       to : this.moniPresale1.address, 
  //       value: web3.utils.toWei('75', 'ether')
  //     }

  //     await this.moniPresale1.sendTransaction(options)
  //   }

  //   let address1DepositAddressAmount = await this.moniPresale1._depositAddressesBNBAmount(address1);
  //   let address2DepositAddressAmount = await this.moniPresale1._depositAddressesBNBAmount(address2);
  //   let address3DepositAddressAmount = await this.moniPresale1._depositAddressesBNBAmount(address3);
  //   let totalDepositAmount = await this.moniPresale1._totalAddressesDepositAmount();

  //   expect(address1DepositAddressAmount).to.be.bignumber.equal(new BN(web3.utils.toWei('50', 'ether')));
  //   expect(address2DepositAddressAmount).to.be.bignumber.equal(new BN(web3.utils.toWei('100', 'ether')));
  //   expect(address3DepositAddressAmount).to.be.bignumber.equal(new BN(web3.utils.toWei('75', 'ether')));
  //   expect(totalDepositAmount).to.be.bignumber.equal(new BN(web3.utils.toWei('225', 'ether')));
  // });

  it('Should allow deposit although total deposit more than 200 bnb', async function () {
    await time.increaseTo(1631059200) // Set to 1 minute just after preSale1 started
    
    {
      const options = { 
        from: address1, 
        to : this.moniPresale1.address, 
        value: web3.utils.toWei('50', 'ether')
      }

      await this.moniPresale1.sendTransaction(options)
    }

    {
      const options = { 
        from: address2, 
        to : this.moniPresale1.address, 
        value: web3.utils.toWei('90', 'ether')
      }

      await this.moniPresale1.sendTransaction(options)
    }

    {
      const options = { 
        from: address3, 
        to : this.moniPresale1.address, 
        value: web3.utils.toWei('75', 'ether')
      }

      await this.moniPresale1.sendTransaction(options)
    }

    const address3BalanceAfter = await web3.eth.getBalance(address3)
   
    let address1DepositAddressAmount = await this.moniPresale1._depositAddressesBNBAmount(address1);
    let address2DepositAddressAmount = await this.moniPresale1._depositAddressesBNBAmount(address2);
    let address3DepositAddressAmount = await this.moniPresale1._depositAddressesBNBAmount(address3);
    let totalDepositAmount = await this.moniPresale1._totalAddressesDepositAmount();

    // expect(address1DepositAddressAmount).to.be.bignumber.equal(new BN(web3.utils.toWei('50', 'ether')));
    // expect(address2DepositAddressAmount).to.be.bignumber.equal(new BN(web3.utils.toWei('90', 'ether')));
    // expect(address3DepositAddressAmount).to.be.bignumber.equal(new BN(web3.utils.toWei('60', 'ether')));
    // expect(totalDepositAmount).to.be.bignumber.equal(new BN(web3.utils.toWei('200', 'ether')));

    // const contractBalance = await web3.eth.getBalance(this.moniPresale1.address)
    // expect(contractBalance).to.be.bignumber.equal(new BN(web3.utils.toWei('200', 'ether')));

  });

  // it('Should not allow deposit if reach cap', async function () {
  //   await time.increaseTo(1631059200) // Set to 1 minute just after preSale1 started
    
  //   {
  //     const options = { 
  //       from: address1, 
  //       to : this.moniPresale1.address, 
  //       value: web3.utils.toWei('50', 'ether')
  //     }

  //     await this.moniPresale1.sendTransaction(options)
  //   }

  //   {
  //     const options = { 
  //       from: address2, 
  //       to : this.moniPresale1.address, 
  //       value: web3.utils.toWei('90', 'ether')
  //     }

  //     await this.moniPresale1.sendTransaction(options)
  //   }

  //   {
  //     const options = { 
  //       from: address3, 
  //       to : this.moniPresale1.address, 
  //       value: web3.utils.toWei('75', 'ether')
  //     }

  //     await this.moniPresale1.sendTransaction(options)
  //   }

  //   {
  //     const options = { 
  //       from: address4, 
  //       to : this.moniPresale1.address, 
  //       value: web3.utils.toWei('2', 'ether')
  //     }

  //     await this.moniPresale1.sendTransaction(options)
  //   }
  //});
});
