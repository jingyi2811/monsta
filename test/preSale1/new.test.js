const { BN, time, expectRevert} = require('@openzeppelin/test-helpers');
const { expect } = require('chai');

const MoniToken = artifacts.require('MoniToken');
const MoniPresale1 = artifacts.require('MoniPresale1');

contract('payable', function (accounts) {
  const [ 
    owner,
    otherAddress, 
    address1,
    address2,
    address3,
    address4,
    address5,
    address6,
    address7,
    address8,
  ] = accounts;

  beforeEach(async function () {
    this.moniToken = await MoniToken.new(otherAddress, otherAddress, otherAddress, otherAddress, otherAddress, otherAddress);
    this.moniPresale1 = await MoniPresale1.new(this.moniToken.address)
    await this.moniToken.setPreSale1ContractNotYetSet(this.moniPresale1.address);

    await this.moniPresale1.addDepositAddress(
      [address1, address2, address3, address4, address5, address6, address7, address8],
      { from: owner, value: '0', gas: '5000000' });
  });

  it('Should allow deposit although total deposit more than 200 bnb', async function () {
    await time.increaseTo(1631059200) // Set to 1 minute just after preSale1 started
    
    {
      const options = { 
        from: address1, 
        to : this.moniPresale1.address, 
        value: web3.utils.toWei('5', 'ether')
      }

      await this.moniPresale1.sendTransaction(options)
    }

    {
      const options = { 
        from: address2, 
        to : this.moniPresale1.address, 
        value: web3.utils.toWei('7', 'ether')
      }

      await this.moniPresale1.sendTransaction(options)
    }

    {
      const options = { 
        from: address3, 
        to : this.moniPresale1.address, 
        value: web3.utils.toWei('3', 'ether')
      }

      await this.moniPresale1.sendTransaction(options)
    }

    {
      const options = { 
        from: address3, 
        to : this.moniPresale1.address, 
        value: web3.utils.toWei('6', 'ether')
      }

      await this.moniPresale1.sendTransaction(options)
    }

    let totalDepositAmount = await this.moniPresale1._depositAddressesAwardedTotalErc20CoinAmount(address3);
    let distribution1 = await this.moniPresale1._depositAddressesAwardedDistribution1Erc20CoinAmount(address3);
    let distribution2 = await this.moniPresale1._depositAddressesAwardedDistribution2Erc20CoinAmount(address3);
    
    console.log(totalDepositAmount.toString())
    console.log(distribution1.toString())
    console.log(distribution2.toString())
  });
});
