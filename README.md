1) Run npm install.

2) Find time.js in the node_modules and comment the following line.

  //if (duration.isNeg()) throw Error(`Cannot increase time by a negative amount (${duration})`);

[Run Ganache-cli]

1) Install ganache-cli
2) Execute ganache-cli --allowUnlimitedContractSize  --gasLimit 0xFFFFFFFFFFFF --defaultBalanceEther 100000 [ Where 100,000 ether will be supplied to 10 default accounts]

[Run truffle test]

1) Go to the test folder of the Truffle directory
2) Run the truffle test with showing event
   For eg: truffle test test/presale2/removeDepositAddress.test.js --show-events